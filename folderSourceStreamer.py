#!/usr/bin/env python

import os,argparse
import cv2
import gi
import numpy as np
import time
from glob import glob

gi.require_version('Gst', '1.0')
from gi.repository import Gst

def on_eos(self, bus, message):
    print('eos message -> {}'.format(message))

def on_error(self, bus, message):
    print('error message -> {}'.format(message.parse_error().debug))


Gst.init(None)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
    description="Stream jpg images from a folder at a desired framerate")
    parser.add_argument("-i",
                        "--inputFolder",
                        help="folder with *.jpg images to show", type=str, default="")
    parser.add_argument("-o",
                        "--outputIp",
                        help="ip to stream to.", type=str, default="")
    parser.add_argument("-p",
                        "--port",
                        help="desired port to stream to", type=int, default=5600)
    parser.add_argument("-W",
                        "--width",
                        help="Width of the image to stream", type=int, default=1920)
    parser.add_argument("-H",
                        "--height",
                        help="Heigt of the image to stream", type=int, default=1080)
    parser.add_argument("-f",
                        "--fps",
                        help="fps", type=int, default=1)
    parser.add_argument("-b",
                        "--bitrate",
                        help="bitrate of the x264 encoder", type=int, default=1000)
    parser.add_argument("-j",
                        "--jetson",
                        help="running on jetson? {0,1}", type=int, default=0)

    args = parser.parse_args()

    assert os.path.isdir(args.inputFolder), "A valid input folder for the images need to be provided!"

    if args.jetson == True:
        command_encoder = 'omxh264enc control-rate=1 bitrate={} iframeinterval=1 '.format(str(args.bitrate*1000))
    else:
        command_encoder = 'x264enc bitrate={} speed-preset=0 key-int-max=1'.format(str(args.bitrate))
    command_sink = ' videoconvert ! queue ! {} ! h264parse ! rtph264pay ! udpsink async=false sync=false host={} port={} '.format(command_encoder,args.outputIp,str(args.port))
    command_src = 'appsrc name=source caps=video/x-raw,format=BGR,width={},height={},framerate={}/1 '.format(str(args.width),str(args.height),str(args.fps))
    command = '{} ! {}'.format(command_src,command_sink )

    print("Start gstreamer pipeline...")
    print(command)

    video_pipe = Gst.parse_launch(command)
    video_pipe.set_state(Gst.State.PLAYING)
    video_src0 = video_pipe.get_child_by_name('source')

    bus = video_src0.get_bus()
    bus.connect('message::error', on_error)
    bus.connect('message::eos', on_eos)

    images = sorted(glob(os.path.join(args.inputFolder,"*.jpg")))
    print("Found ", len(images), " images!")
    i = 0.0
    sleep = 1.0/args.fps
    while True:
        for oneImg in images:
            print(i)
            
            #read from cv2
            img = cv2.imread(oneImg,cv2.IMREAD_COLOR)
            img = cv2.resize(img, (args.width,args.height), interpolation=cv2.INTER_AREA) # resized frame
            #cv2.imshow("aa",img)
            #if cv2.waitKey(1) & 0xFF == ord('q'):
            #    break

            #convert to gst
            data = img.tostring()
            buf = Gst.Buffer.new_allocate(None, len(data), None)
            assert buf is not None
            buf.fill(0, data)
            
            #Set timestamp
            #buf.pts = buf.dts = int(i * sleep * 1e9)
            clock = video_src0.get_clock()
            base_time = video_src0.base_time
            abs_time = clock.get_time()
            buf.pts = buf.dts = abs_time - base_time

            #send data
            retval = video_src0.emit("push-buffer", buf)
            if retval != Gst.FlowReturn.OK:
                print(retval)

            time.sleep(sleep)
            i=i+1.0


    video_src0.emit("end-of-stream")

    print("Bye")
