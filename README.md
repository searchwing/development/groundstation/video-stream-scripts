# Video-stream-scripts

Some tools to receive and process video streams. Mainly used with WifiBroadcast video / image data streamed via gstreamer rtp packets.

## DroneVideoStreamImageDumper

A script to convert incoming video stream data via gstreamer (e.g. from wifibroadcast) to single images. 
The images will be filled with metadata using a additional mavlink connection (e.g. from wifibroadcast).
See help from the script for further info.

```
./droneVideoStreamImageDumper.py -h
usage: droneVideoStreamImageDumper.py [-h] [-p PORT] [-s MAVLINKSOURCE] [-o OUTFOLDER] [-q QUALITY] [-m {0,1}] [-v {0,1}]

Dump all images from a given gstreamer video stream from localhost via udpsource as jpeg. Optional add metadata from a mavlink stream to the images.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTIPADDRESS, --inputIpAddress INPUTIPADDRESS
                        IP of the gstreamer source.
  -p PORT, --port PORT  UDP-Port for the gstreamer source.
  -m {0,1}, --mavlinkUse {0,1}
                        Use Mavlink to add metadata.
  -s MAVLINKSOURCE, --mavlinkSource MAVLINKSOURCE
                        Mavlink source string (e.g. 127.0.0.1:14550 or /dev/ttyUSB0)
  -o OUTFOLDER, --outFolder OUTFOLDER
                        Destitation of the images.
  -q QUALITY, --quality QUALITY
                        Jpeg quality of the saved images.
  -v {0,1}, --view {0,1}
                        View live preview frames.
```

## FolderSourceStreamer
```
./folderSourceStreamer.py -h
usage: folderSourceStreamer.py [-h] [-i INPUTFOLDER] [-o OUTPUTIP] [-p PORT] [-W WIDTH] [-H HEIGHT] [-f FPS] [-b BITRATE] [-j JETSON]

Stream jpg images from a folder at a desired framerate

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTFOLDER, --inputFolder INPUTFOLDER
                        folder with *.jpg images to show
  -o OUTPUTIP, --outputIp OUTPUTIP
                        ip to stream to.
  -p PORT, --port PORT  desired port to stream to
  -W WIDTH, --width WIDTH
                        Width of the image to stream
  -H HEIGHT, --height HEIGHT
                        Heigt of the image to stream
  -f FPS, --fps FPS     fps
  -b BITRATE, --bitrate BITRATE
                        bitrate of the x264 encoder
  -j JETSON, --jetson JETSON
                        running on jetson? {0,1}
```

