# multi src from julian
#gst-launch-1.0 -v -e multifilesrc location="%04d.jpg" index=0 caps="image/jpeg,framerate=1/1" loop=1 ! decodebin ! videoscale ! videorate ! video/x-raw,width=1920,height=1080 ! x264enc bitrate=1000 speed-preset=0 key-int-max=1 ! h264parse ! rtph264pay ! udpsink async=false sync=true host=127.0.0.1 port=5600

# camera src from julian
#gst-launch-1.0 -e nvarguscamerasrc sensor_id=0 ! 'video/x-raw(memory:NVMM),width=1920, height=1080, framerate=22/1' ! omxh264enc control-rate=2 bitrate=4000000 ! video/x-h264, stream-format=byte-stream ! rtph264pay mtu=1400 ! udpsink async=false sync=false host=192.168.1.1 port=5600

gst-launch-1.0 -v -e multifilesrc location="%04d.jpg" index=0 caps="image/jpeg,framerate=\(fraction\)1/1" loop=1 ! \
	jpegdec ! \
	videoscale ! \
	videorate ! \
	video/x-raw,width=1920,height=1080,framerate=2/1 ! \
	clockoverlay ! \
	omxh264enc bitrate=4000000 iframeinterval=1 ! \
	video/x-h264, stream-format=byte-stream ! \
	rtph264pay mtu=1530 ! \
	udpsink async=false sync=true host=192.168.1.1 port=5600

