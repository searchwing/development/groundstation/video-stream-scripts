gst-launch-1.0 -v videotestsrc pattern=ball ! \
	clockoverlay ! videoconvert ! videorate ! \
	video/x-raw,width=1920,height=1080,framerate=1/1 ! \
	omxh264enc bitrate=1000000 ! \
	video/x-h264, stream-format=byte-stream ! \
	rtph264pay mtu=1400 ! \
	udpsink async=false sync=true host=192.168.1.1 port=5600
