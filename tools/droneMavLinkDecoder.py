
from pymavlink import mavutil
import threading
import datetime
import time
import logging
import sys

class DroneMavlinkDecoder():
	def __init__(self, source, data_buf_max_size = 300):
		self.init_logger("INFO")

		#msg ids
		self.GPSMSGBUFID = 1

		#buffer
		self.data_buf = []
		self.data_buf_max_size = data_buf_max_size

		#stamps
		self.basestationStartStamp = None
		self.droneStartStamp = None
		self.droneStartTimeBootStamp = None
		
		#mavlink
		self.MAVLINK_CHECK_PERIODE = 0.01
		self.master = mavutil.mavlink_connection(source)  # drone connection
		self.mavlink_thread = threading.Thread(target=self.mavlink_loop)

		#threads		
		self.threadDataExchangeLock = threading.Lock()
		self.mavlink_thread.start()

	def init_logger(self,LOGGER_LEVEL):
		self.logger = logging.getLogger()
		if LOGGER_LEVEL == "INFO":
			self.logger.setLevel(logging.INFO)
		elif LOGGER_LEVEL == "DEBUG":
			self.logger.setLevel(logging.DEBUG)
		elif LOGGER_LEVEL == "ERROR":
			self.logger.setLevel(logging.ERROR)
		else:
			logger.setLevel(logging.INFO)

		consoleHandler = logging.StreamHandler(sys.stdout)
		#consoleHandler.setFormatter(formatter) ### add possible stamp formater
		self.logger.addHandler(consoleHandler)

	def mavlink_loop(self):

		time_received = False

		msg_types = ['SYSTEM_TIME', 'GLOBAL_POSITION_INT']

		while True:
			time.sleep(self.MAVLINK_CHECK_PERIODE)
			msg = None
			while True:
				msg = self.master.recv_match(type=msg_types, blocking=False)
				if msg is None:
					break
				msgtype = msg.get_type()

				# Timestamp based on GPS
				"""get RTC time from ardupilot which is UTC if following is set to GPS:
				http://ardupilot.org/copter/docs/parameters.html#brd-rtc-types-allowed-sources-of-rtc-time
				The utc time can be aquired by reading the RTC based SYSTEM_TIME via MAVLINK:
				https://github.com/ArduPilot/ardupilot/blob/378d5c7a5abd7ed2d96f00bcc63f0dbabbc523e9/libraries/GCS_MAVLink/GCS_Common.cpp#L1488
				"""
				if msgtype == 'SYSTEM_TIME' and time_received == False:
					now_groundstation = datetime.datetime.now()

					now_drone = datetime.datetime.fromtimestamp(msg.time_unix_usec/1000000)
					self.logger.debug("[SYSTEM_TIME] drone time: {}".format(now_drone.isoformat()))

					if now_drone.year >= 2020: # rudimentary validity check
						self.basestationStartStamp  = now_groundstation
						self.droneStartStamp = now_drone
						droneStartTimeBootSec = datetime.timedelta( seconds = msg.time_boot_ms/1000.0)
						self.droneStartTimeBootStamp = now_drone - droneStartTimeBootSec

						time_received = True
					else:
						self.logger.debug("[SYSTEM_TIME] Discard Pixracer timestamp as it is not valid (year < 2020!)")

				# GPS 
				elif msgtype == 'GLOBAL_POSITION_INT':
					if time_received:
						self.add_new_data_to_buffer_gps(msg)
				
				# limit buffer
				while len(self.data_buf) > self.data_buf_max_size:
					self.data_buf.pop(0)
						

	def time_boot_ms_2_UTC_stamp(self,time_boot_ms):
		return self.droneStartTimeBootStamp + datetime.timedelta( seconds = time_boot_ms/1000.0)
	
	def add_new_data_to_buffer_gps(self,msg):
		self.logger.debug("[MAVLINK GPS msg]:(lat:{}, lon:{}, hdg:{}°)".format(msg.lat / 10e6, msg.lon / 10e6, msg.hdg / 10e1 ))
		new_data = [self.GPSMSGBUFID, self.time_boot_ms_2_UTC_stamp(msg.time_boot_ms), msg.lat / 10e6, msg.lon / 10e6, msg.alt / 10e3, msg.hdg / 10e2 ]
		
		self.threadDataExchangeLock.acquire()
		self.data_buf.append(new_data)
		self.threadDataExchangeLock.release()

	def get_nearest_data_to_timestamp(self, msgid, timestamp):
		self.threadDataExchangeLock.acquire()

		if len(self.data_buf) < 1 or timestamp > self.data_buf[-1][1]:
			self.threadDataExchangeLock.release()
			return None

		lastData = None
		nextData = None
		for oneData in self.data_buf:
			if oneData[0]  == msgid:
				if oneData[1] > timestamp:
					nextData = oneData
					break
				lastData = oneData
		
		self.threadDataExchangeLock.release()

		if lastData == None: # case when just one data in buffer
			return nextData
		
		dt0 = abs(timestamp - lastData[1])
		dt1 = abs(timestamp - nextData[1])

		if dt0 < dt1:
			return lastData
		else:
			return nextData
		
		
if __name__ == '__main__':
	SIMULATION_SITL_ADDRESS = "127.0.0.1:14550"
	dec = DroneMavlinkDecoder(SIMULATION_SITL_ADDRESS)

	while True:
		time.sleep(0.1)
		now= datetime.datetime.now()
		datastamp = now + datetime.timedelta( seconds = -15.5)
		print(dec.get_nearest_data_to_timestamp(1,now))



	

