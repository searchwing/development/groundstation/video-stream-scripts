import piexif
import piexif.helper
from fractions import Fraction


class ExifWriter():
    @staticmethod
    # from https://gist.github.com/NeoFarz/27f35ec2f84f5c52394cea3891c18832
    def to_deg(value, loc):
    	"""convert decimal coordinates into degrees, munutes and seconds tuple
    	Keyword arguments: value is float gps-value, loc is direction list ["S", "N"] or ["W", "E"]
    	return: tuple like (25, 13, 48.343 ,'N')
    	"""
    	if value < 0:
    		loc_value = loc[0]
    	elif value > 0:
    		loc_value = loc[1]
    	else:
    		loc_value = ""
    	abs_value = abs(value)
    	deg =  int(abs_value)
    	t1 = (abs_value-deg)*60
    	min = int(t1)
    	sec = round((t1 - min)* 60, 5)
    	return (deg, min, sec, loc_value)

    @staticmethod
    def change_to_rational(number):
    	"""convert a number to rantional
    	Keyword arguments: number
    	return: tuple like (1, 2), (numerator, denominator)
    	"""
    	f = Fraction(str(number))
    	return (f.numerator, f.denominator)

    """Slow method for writing exif data when using simulation
    """
    @staticmethod
    def write_exif_metadata(file_path: str, lat: float, lng: float, altitude: float, heading: float, stamp):
    	"""Adds GPS position + local pose as EXIF metadata
    	"""
    	#TODO: change existing exif data instead of overwriting it

    	lat_deg = ExifWriter.to_deg(lat, ["S", "N"])
    	lng_deg = ExifWriter.to_deg(lng, ["W", "E"])

    	exiv_lat = (ExifWriter.change_to_rational(lat_deg[0]), ExifWriter.change_to_rational(lat_deg[1]), ExifWriter.change_to_rational(lat_deg[2]))
    	exiv_lng = (ExifWriter.change_to_rational(lng_deg[0]), ExifWriter.change_to_rational(lng_deg[1]), ExifWriter.change_to_rational(lng_deg[2]))

    	gps_ifd = {
    		piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
    		piexif.GPSIFD.GPSAltitudeRef: 0,
    		piexif.GPSIFD.GPSAltitude: ExifWriter.change_to_rational(round(altitude)),
    		piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
    		piexif.GPSIFD.GPSLatitude: exiv_lat,
    		piexif.GPSIFD.GPSLongitudeRef: lng_deg[3],
    		piexif.GPSIFD.GPSLongitude: exiv_lng,
    		piexif.GPSIFD.GPSImgDirectionRef: "T", # T=True North, M=Magnetic North
    		piexif.GPSIFD.GPSImgDirection: ExifWriter.change_to_rational(float('%.2f'%(heading))) # only save with 2 decimal precessions
    	}
    	exif_ifd = {
    		piexif.ExifIFD.DateTimeOriginal: stamp.strftime("%Y:%m:%d %H:%M:%S"),
    		piexif.ExifIFD.SubSecTimeOriginal: stamp.strftime("%f")
    	}
    	exif_dict = {"Exif":exif_ifd,"GPS": gps_ifd}
    	    	
    	exif_bytes = piexif.dump(exif_dict)
    	piexif.insert(exif_bytes, file_path)