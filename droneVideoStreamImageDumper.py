#!/usr/bin/python3 

import time
from collections import defaultdict
import argparse
import os

import cv2
import gi
import numpy as np

gi.require_version('Gst', '1.0')  # isort:skip
from gi.repository import Gst  # isort:skip

from tools.droneMavLinkDecoder import DroneMavlinkDecoder
from tools.exifWriter import ExifWriter
import datetime
import threading


threadDataExchangeLock = threading.Lock()

class DroneVideoStreamImageDumper():
    def __init__(self,ip,udpport,mavlinkSource,outFolder,quality,mavlinkUse):
        #settings
        self.mavlinkUse = mavlinkUse
        if mavlinkUse:
            self.mavlinkDecoder = DroneMavlinkDecoder(mavlinkSource)
        self.outFolder = outFolder

        self.frameBufferMaxSize = 100
        self.frameBuffer = []
        self.frames = dict()
        self.fps = defaultdict(lambda: dict(timestamp=time.time(), num_frames=0))
        self.jpg_counter = -1

        #threads
        self.frameBuffer_writer_thread = threading.Thread(target=self.buffer_writer)
        self.frameBuffer_writer_thread.start()

        #gstreamer source
        Gst.init(None)
        command_source = "udpsrc address={} port={} ! application/x-rtp, encoding-name=H264, media=(string)video , clock-rate=(int)90000 ! rtph264depay ! queue ! avdec_h264".format(ip,str(udpport)) # payload=96
        command_sink = 'appsink emit-signals=true sync=false max-buffers=10 drop=false'
        command_jpeg = 'videoconvert ! jpegenc quality={} ! {}'.format(str(quality),command_sink)
        command = '{} ! queue ! {} '.format(command_source, command_jpeg)
        print("Start gstreamer pipeline...")
        print(command)

        self.video_pipe = Gst.parse_launch(command)
        self.video_pipe.set_state(Gst.State.PLAYING)
        self.video_sink0 = self.video_pipe.get_by_name('appsink0')

        self.video_sink0.connect('new-sample', self.callback_jpeg)

    def buffer_writer(self):
        while True:
            frames_to_remove = []
            
            #go through all images in buffer 
            for idx,oneFrame in enumerate(self.frameBuffer):
                stamp = oneFrame[0]

                lat = None
                lon = None
                alt = None
                hdg = None

                if self.mavlinkUse:
                    #check if there is gps info for the stamps available
                    gpsPosData = self.mavlinkDecoder.get_nearest_data_to_timestamp(self.mavlinkDecoder.GPSMSGBUFID, stamp)
                    if gpsPosData == None:
                        continue
                    else:
                        print(gpsPosData)
                        lat = gpsPosData[2]
                        lon = gpsPosData[3]
                        alt = gpsPosData[4]
                        hdg = gpsPosData[5]
                
                filename = stamp.strftime("%Y-%m-%dT%H-%M-%S.%f") + ".jpg"
                #write image to disk
                jpgBuf = oneFrame[1]
                filename = '{}/{}'.format(self.outFolder,filename)
                with open(filename, 'wb') as fp:
                    fp.write(jpgBuf)
                print("saved {}".format(filename))
                #write exif metadata to image on disk
                if self.mavlinkUse:
                    ExifWriter.write_exif_metadata(filename, lat, lon, alt,hdg, stamp)

                frames_to_remove.append(idx)
            
            threadDataExchangeLock.acquire()
            # remove written images from buffer
            for index in sorted(frames_to_remove, reverse=True):
                del self.frameBuffer[index]
            # limit buffer
            while len(self.frameBuffer) > self.frameBufferMaxSize :
                self.frameBuffer.pop(0)
            threadDataExchangeLock.release()

            time.sleep(0.5)

    def callback_jpeg(self, sink):
        stamp = datetime.datetime.now()

        #get image via gst
        sample = sink.emit('pull-sample')
        buf = sample.get_buffer()
        bufData = buf.extract_dup(0, buf.get_size())

        #write frame to buffer
        threadDataExchangeLock.acquire()
        self.frameBuffer.append([stamp,bufData])
        threadDataExchangeLock.release()
        self.frames['jpeg'] = bufData

        #calc fps
        self.fps['jpeg']['num_frames'] += 1
        if self.fps['jpeg']['num_frames'] % 100 == 0:
            now = time.time()
            time_passed = now - self.fps['jpeg']['timestamp']
            print('FPS jpeg: {}'.format(self.fps['jpeg']['num_frames'] / time_passed))
            self.fps['jpeg']['timestamp'] = now
            self.fps['jpeg']['num_frames'] = 0

        return Gst.FlowReturn.OK


    def draw_frame(self, key):
        if key in self.frames:
            frame = self.frames[key]
            if key == "jpeg":
                frame = cv2.imdecode(np.fromstring(frame, dtype=np.uint8), 1)
            if frame is not None and frame.shape[0] > 0 and frame.shape[1] > 0:
                cv2.imshow(key, frame)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
    description="Dump all images from a given gstreamer video stream from localhost via udpsource as jpeg. Optional add metadata from a mavlink stream to the images.")
    parser.add_argument("-i",
                        "--inputIpAddress",
                        help="IP of the gstreamer source.", type=str, default="127.0.0.1")
    parser.add_argument("-p",
                        "--port",
                        help="UDP-Port for the gstreamer source.", type=int, default=5600)
    parser.add_argument("-m",
                        "--mavlinkUse",
                        help="Use Mavlink to add metadata.", type=int, default=1,choices=[0,1])
    parser.add_argument("-s",
                        "--mavlinkSource",
                        help="Mavlink source string (e.g. 127.0.0.1:14550 or /dev/ttyUSB0)", type=str, default="127.0.0.1:14550")
    parser.add_argument("-o",
                        "--outFolder",
                        help="Destitation of the images.", type=str, default="out")
    parser.add_argument("-q",
                        "--quality",
                        help="Jpeg quality of the saved images.", type=int, default=99)
    parser.add_argument("-v",
                        "--view",
                        help="View live preview frames.", type=int, default=0,choices=[0,1])

    args = parser.parse_args()

    assert os.path.isdir(args.outFolder), "A valid output folder for the images need to be provided!"

    grabber = DroneVideoStreamImageDumper(args.inputIpAddress,args.port,args.mavlinkSource,args.outFolder,args.quality,args.mavlinkUse)

    if args.view:
        cv2.namedWindow('jpeg', cv2.WINDOW_NORMAL)

    while True:
        if args.view:
            grabber.draw_frame('jpeg')

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        time.sleep(0.1)
